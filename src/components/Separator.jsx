import wave from "../assets/wave1.png"

const Separator = () => (
    <div className="separator">
        <img src={wave} alt="wave" className="wave" />
    </div>
)

export default Separator
